export interface BoardItem {
  title: string;
  comments: Array<string>;
  likes: number;
  important: boolean;
  id?: number;
}
