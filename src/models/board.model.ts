import { BoardItem } from './boardItem.model';

type Board = {
  id?: string;
  title: string;
  color: string;
  items: Array<BoardItem>;
};

export default Board;
