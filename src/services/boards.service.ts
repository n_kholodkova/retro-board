import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BoardItem } from './../models/boardItem.model';
import { map } from 'rxjs/operators';
import { Observable, Subject, from } from 'rxjs';
import * as XLSX from 'xlsx';

import Board from './../models/board.model';

@Injectable({
  providedIn: 'root',
})
export class BoardsService {
  defaultColor = '#e92c64';

  constructor(private http: HttpClient) {}

  transformData = (
    dataObj: { title: string; color: string; items: Array<BoardItem> },
    key: string
  ): Board => ({ ...dataObj, id: key, items: dataObj.items ?? [] });

  makePutRequest = (boardId: string, currentBoard: Board) =>
    this.http.put(
      `https://retroboard-b0d42-default-rtdb.firebaseio.com/boards/${boardId}.json`,
      currentBoard
    );

  getBoards = () =>
    this.http
      .get('https://retroboard-b0d42-default-rtdb.firebaseio.com/boards.json')
      .pipe(
        map((data: any) => {
          const dataArray = [];
          for (let key in data) {
            if (data.hasOwnProperty(key)) {
              dataArray.push(this.transformData(data[key], key));
            }
          }
          return dataArray;
        })
      );

  getBoard = (id: string): Observable<Board> =>
    this.http.get<Board>(
      `https://retroboard-b0d42-default-rtdb.firebaseio.com/boards/${id}.json`
    );

  addBoardItem = (board: Board, title: string, id: string) => {
    const boardItem = {
      title,
      comments: [],
      likes: 0,
      important: false,
      id: Date.now(),
    };
    if (!board.items) {
      board.items = [];
    }
    board.items.push(boardItem);
    return this.makePutRequest(id, board);
  };

  addBoard = (title: string) =>
    this.http.post(
      'https://retroboard-b0d42-default-rtdb.firebaseio.com/boards.json',
      { title, color: this.defaultColor, items: [] }
    );

  deleteBoardItem = (index: number, board: Board, id: string) => {
    board.items = board.items.filter((item, i) => i !== index);
    return this.makePutRequest(id, board);
  };

  deleteBoard = (id: string) =>
    this.http.delete(
      `https://retroboard-b0d42-default-rtdb.firebaseio.com/boards/${id}.json`
    );

  addComment = (
    index: number,
    title: string,
    currentBoard: Board,
    id: string
  ) => {
    if (!currentBoard.items[index].comments) {
      currentBoard.items[index].comments = [];
    }
    currentBoard.items[index].comments.push(title);
    return this.makePutRequest(id, currentBoard);
  };

  like = (index: number, currentBoard: Board, id: string) => {
    currentBoard.items[index].likes += 1;
    return this.makePutRequest(id, currentBoard);
  };

  dislike = (index: number, currentBoard: Board, id: string) => {
    currentBoard.items[index].likes -= 1;
    return this.makePutRequest(id, currentBoard);
  };

  markImportant = (index: number, currentBoard: Board, id: string) => {
    currentBoard.items[index].important = !currentBoard.items[index].important;
    return this.makePutRequest(id, currentBoard);
  };

  changeColor = (currentBoard: Board, color: string, id: string) => {
    currentBoard.color = color;
    return this.makePutRequest(id, currentBoard);
  };

  updateDB = (boards: Board[]) => {
    for (let board of boards) {
      this.makePutRequest(board?.id ?? '', board).subscribe();
    }
  };

  exportAsExcelFile = (boards: Board[]): void => {
    const xlsData = [];
    for (let board of boards) {
      for (let item of board.items) {
        xlsData.push({
          title: board.title,
          task: item.title,
          likes: item.likes,
          importance: item.important,
        });
      }
    }
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(xlsData);
    const workbook: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(workbook, worksheet, 'boards');
    XLSX.writeFile(workbook, 'boardsData.xlsx', { type: 'file' });
  };
}
