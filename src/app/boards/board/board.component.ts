import { Component, Input, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { BoardItem } from './../../../models/boardItem.model';

import { BoardStateType } from '../../../store/state';
import {
  LikeAction,
  DislikeAction,
  ImportantAction,
  DeleteBoardItemAction,
} from '../../../store/actions';

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.scss'],
})
export class BoardComponent implements OnInit {
  @Input()
  boardItem!: BoardItem;

  @Input()
  currentIndex!: number;

  @Input()
  boardId!: string;

  @Input()
  color!: string;

  isCommentsOpened = false;

  constructor(private store: Store<BoardStateType>) {}

  ngOnInit(): void {}

  handleLike = () => {
    this.store.dispatch(
      LikeAction({
        payload: { id: this.boardId, index: this.currentIndex },
      })
    );
  };

  handleDisLike = () => {
    this.store.dispatch(
      DislikeAction({
        payload: { id: this.boardId, index: this.currentIndex },
      })
    );
  };

  handleDelete = () => {
    this.store.dispatch(
      DeleteBoardItemAction({
        payload: { id: this.boardId, index: this.currentIndex },
      })
    );
  };

  handleImportant = () => {
    this.store.dispatch(
      ImportantAction({
        payload: { id: this.boardId, index: this.currentIndex },
      })
    );
  };

  handleOpenCommentsButton = () => {
    this.isCommentsOpened = !this.isCommentsOpened;
  };
}
