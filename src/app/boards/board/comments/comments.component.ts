import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.scss'],
})
export class CommentsComponent implements OnInit {
  @Input() comments: Array<string> = [];
  @Input()
  boardId!: string;
  @Input()
  boardItemIndex!: number;

  constructor() {}

  ngOnInit(): void {}
}
