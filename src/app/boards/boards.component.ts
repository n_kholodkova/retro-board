import { Component, Input, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import Board from './../../models/board.model';

import { BoardsService } from '../../services/boards.service';
import { BoardStateType } from '../../store/state';
import { SetBoardsAction } from '../../store/actions';

@Component({
  selector: 'app-boards',
  templateUrl: './boards.component.html',
  styleUrls: ['./boards.component.scss'],
})
export class BoardsComponent implements OnInit {
  @Input() boards: Board[] = [];
  isTaskFormOpened = false;

  constructor(
    private store: Store<BoardStateType>,
    private boardsService: BoardsService
  ) {}

  ngOnInit(): void {}

  updateDB = (data: any) => {
    const {
      targetContainer,
      prevContainer,
      prevContainerData,
      targContainerData,
    } = data;
    const targetIdx = this.boards.findIndex((board) =>
      board.items.every((item) =>
        targContainerData.map((item: Board) => item.id).includes(item.id)
      )
    );
    const prevIdx = this.boards.findIndex((board, i) =>
      board.items.every((item) => prevContainerData
          .map((item: Board) => item.id)
          .includes(item.id)
      )
    );
    const newBoards = [...this.boards];
    newBoards[targetIdx] = {
      ...this.boards[targetIdx],
      items: targetContainer,
    };
    newBoards[prevIdx] = { ...this.boards[prevIdx], items: prevContainer };
    this.boardsService.updateDB([newBoards[targetIdx], newBoards[prevIdx]]);
    this.store.dispatch(
      SetBoardsAction({
        payload: { boards: newBoards },
      })
    );
  };
}
