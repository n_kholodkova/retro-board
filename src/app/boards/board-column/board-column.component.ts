import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Store } from '@ngrx/store';
import {
  CdkDragDrop,
  moveItemInArray,
  transferArrayItem,
} from '@angular/cdk/drag-drop';

import Board from './../../../models/board.model';
import { BoardItem } from './../../../models/boardItem.model';

import { BoardStateType } from '../../../store/state';
import { DeleteBoardAction } from '../../../store/actions';

@Component({
  selector: 'app-board-column',
  templateUrl: './board-column.component.html',
  styleUrls: ['./board-column.component.scss'],
})
export class BoardColumnComponent implements OnInit {
  isTaskFormOpened = false;
  @Input()
  board!: Board;

  @Output() changeBoardEmitter = new EventEmitter<any>();

  constructor(private store: Store<BoardStateType>) {}

  ngOnInit(): void {}

  handleDeleteBoard = () => {
    this.store.dispatch(
      DeleteBoardAction({
        payload: { id: this.board?.id ?? '' },
      })
    );
  };

  handleOpenFormButton = () => {
    this.isTaskFormOpened = true;
  };

  handleCloseForm = (data: boolean) => {
    this.isTaskFormOpened = data;
  };

  drop(event: CdkDragDrop<Array<BoardItem>>) {
    const targetContainer = [...event.container.data];
    const prevContainer = [...event.previousContainer.data];
    const prevIdx = event.previousIndex;
    const currIdx = event.currentIndex;
    if (event.previousContainer === event.container) {
      moveItemInArray(targetContainer, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(
        prevContainer,
        targetContainer,
        event.previousIndex,
        event.currentIndex
      );
    }
    this.changeBoardEmitter.emit({
      targetContainer,
      prevContainer,
      targContainerData: event.container.data,
      prevContainerData: event.previousContainer.data,
    });
  }
}
