import { Component, OnInit, Input } from '@angular/core';
import { Store } from '@ngrx/store';

import { BoardStateType } from '../../../../store/state';
import { ChangeColorAction } from '../../../../store/actions';

@Component({
  selector: 'app-color-pick',
  templateUrl: './color-pick.component.html',
  styleUrls: ['./color-pick.component.scss'],
})
export class ColorPickComponent implements OnInit {
  colors = [
    '#e92c64',
    '#4E9F3D',
    '#208eed',
    '#912f84',
    '#141E61',
    '#FF4C29',
    '#610094',
    '#29A19C',
    '#F6C90E',
  ];

  @Input()
  boardId!: string;

  constructor(private store: Store<BoardStateType>) {}

  ngOnInit(): void {}

  changeColorHandler(color: string) {
    this.store.dispatch(
      ChangeColorAction({
        payload: { id: this.boardId, color },
      })
    );
  }
}
