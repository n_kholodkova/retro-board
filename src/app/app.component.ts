import { boardsReducer } from './../store/reducer';
import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import Board from '../models/board.model';
import { BoardsService } from '../services/boards.service';
import { BoardStateType } from '../store/state';
import { GetBoardsAction } from '../store/actions';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  title = 'retro-board';

  isAddBoardFormOpened = false;
  boards: Board[] = [];

  constructor(
    private store: Store<BoardStateType>,
    private boardsService: BoardsService
  ) {}

  setAddForm = (data: boolean) => {
    this.isAddBoardFormOpened = data;
  };

  exportData = (data: boolean) => {
    this.boardsService.exportAsExcelFile(this.boards);
  };

  ngOnInit(): void {
    this.store.subscribe((data: any) => {
      this.boards = [...data.reducer.boards];
    });
    this.store.dispatch(GetBoardsAction());
  }
}
