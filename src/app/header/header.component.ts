import { Component, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  constructor() {}

  @Output() openAddBoardForm = new EventEmitter<boolean>();

  @Output() exportToExcel = new EventEmitter<boolean>();

  ngOnInit(): void {}

  addBoardFormHandler = () => {
    this.openAddBoardForm.emit(true);
  };

  exportHandler = () => {
    this.exportToExcel.emit(true);
  };
}
