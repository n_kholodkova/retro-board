import { NgModule } from '@angular/core';

import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {MatCardModule} from '@angular/material/card';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatInputModule} from '@angular/material/input';
import {DragDropModule} from '@angular/cdk/drag-drop';



@NgModule({
  imports: [
      MatToolbarModule,
      MatIconModule,
      MatButtonModule,
      MatCardModule,
      MatGridListModule,
      MatInputModule,
      DragDropModule,
  ],
  exports: [
      MatToolbarModule,
      MatIconModule,
      MatButtonModule,
      MatCardModule,
      MatGridListModule,
      MatInputModule,
      DragDropModule,
  ]
})
export class MaterialModuleModule { }
