import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { BoardStateType } from './../../../store/state';
import {
  AddBoardAction,
  AddBoardItemAction,
  AddCommentAction,
} from './../../../store/actions';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss'],
})
export class FormComponent implements OnInit {
  form!: FormGroup;
  @Input()
  contentType!: string;
  @Input()
  class!: string;
  @Input()
  boardId!: string;
  @Input()
  boardItemIndex!: number;
  label = '';
  @Output() setCanCloseForm = new EventEmitter<boolean>();

  constructor(private store: Store<BoardStateType>) {}

  submitHandler = () => {
    switch (this.contentType) {
      case 'ADD_BOARD':
        this.store.dispatch(
          AddBoardAction({ payload: { title: this.form.value.data } })
        );
        break;
      case 'ADD_TASK':
        this.store.dispatch(
          AddBoardItemAction({
            payload: { id: this.boardId, title: this.form.value.data },
          })
        );
        break;
      case 'ADD_COMMENT':
        this.store.dispatch(
          AddCommentAction({
            payload: {
              id: this.boardId,
              title: this.form.value.data,
              index: this.boardItemIndex,
            },
          })
        );
        break;
      default:
        break;
    }
    this.setCanCloseForm.emit(false);
  };

  closeFormHandler = () => {
    this.setCanCloseForm.emit(false);
  };

  ngOnInit(): void {
    this.form = new FormGroup({
      data: new FormControl(null, Validators.required),
    });
    switch (this.contentType) {
      case 'ADD_BOARD':
        this.label = 'Board';
        break;
      case 'ADD_TASK':
        this.label = 'Task';
        break;
      case 'ADD_COMMENT':
        this.label = 'Comment';
        break;
      default:
        break;
    }
  }
}
