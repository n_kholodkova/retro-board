import { MaterialModuleModule } from './material-module/material-module.module';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HeaderComponent } from './header/header.component';
import { BoardsComponent } from './boards/boards.component';
import { BoardComponent } from './boards/board/board.component';
import { ReactiveFormsModule } from '@angular/forms';
import { FormComponent } from './shared/form/form.component';
import { BoardColumnComponent } from './boards/board-column/board-column.component';
import { CommentsComponent } from './boards/board/comments/comments.component';
import { ColorPickComponent } from './boards/board-column/color-pick/color-pick.component';
import { HttpClientModule } from '@angular/common/http';

import { boardsReducer } from './../store/reducer';
import { BoardsEffects } from './../store/effects';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    BoardsComponent,
    BoardComponent,
    FormComponent,
    BoardColumnComponent,
    CommentsComponent,
    ColorPickComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModuleModule,
    ReactiveFormsModule,
    HttpClientModule,
    StoreModule.forRoot({ reducer: boardsReducer }),
    EffectsModule.forRoot([BoardsEffects]),
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
