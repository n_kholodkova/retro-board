import Board from 'src/models/board.model';
import { createAction, props } from '@ngrx/store';

export enum BoardsActionsEnum {
  getBoards = 'GET_BOARDS',
  getBoard = 'GET_BOARD',
  setBoards = 'SET_BOARDS',
  addBoardItem = 'ADD_BOARD_ITEM',
  addBoard = 'ADD_BOARD',
  deleteBoardItem = 'DELETE_BOARD_ITEM',
  deleteBoard = 'DELETE_BOARD',
  addComment = 'ADD_COMMENT',
  like = 'LIKE',
  dislike = 'DISLIKE',
  markImportant = 'MARK_IMPORTANT',
  changeColor = 'CHANGE_COLOR',
}

export const GetBoardsAction = createAction(BoardsActionsEnum.getBoards);

export const GetBoardAction = createAction(
  BoardsActionsEnum.getBoard,
  props<{ payload: { id: string } }>()
);

export const SetBoardsAction = createAction(
  BoardsActionsEnum.setBoards,
  props<{ payload: { boards: Board[] } }>()
);

export const SetBoardAction = createAction(
  BoardsActionsEnum.setBoards,
  props<{ payload: { board: Board } }>()
);

export const AddBoardItemAction = createAction(
  BoardsActionsEnum.addBoardItem,
  props<{ payload: { id: string; title: string } }>()
);

export const AddBoardAction = createAction(
  BoardsActionsEnum.addBoard,
  props<{ payload: { title: string } }>()
);

export const DeleteBoardAction = createAction(
  BoardsActionsEnum.deleteBoard,
  props<{ payload: { id: string } }>()
);

export const DeleteBoardItemAction = createAction(
  BoardsActionsEnum.deleteBoardItem,
  props<{ payload: { id: string; index: number } }>()
);

export const AddCommentAction = createAction(
  BoardsActionsEnum.addComment,
  props<{ payload: { title: string; id: string; index: number } }>()
);

export const LikeAction = createAction(
  BoardsActionsEnum.like,
  props<{ payload: { id: string; index: number } }>()
);

export const DislikeAction = createAction(
  BoardsActionsEnum.dislike,
  props<{ payload: { id: string; index: number } }>()
);

export const ImportantAction = createAction(
  BoardsActionsEnum.markImportant,
  props<{ payload: { id: string; index: number } }>()
);

export const ChangeColorAction = createAction(
  BoardsActionsEnum.changeColor,
  props<{ payload: { id: string; color: string } }>()
);
