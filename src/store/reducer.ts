import { initialState } from './state';
import { SetBoardsAction } from './actions';
import { createReducer, on } from '@ngrx/store';

export const boardsReducer = createReducer(
  initialState,
  on(SetBoardsAction, (state, { payload }) => {
    return { ...state, boards: payload.boards }}),
);
