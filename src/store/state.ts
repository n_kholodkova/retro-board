import Board from '../models/board.model';

export type BoardStateType = {
  boards: Array<Board>;
};

export const initialState: BoardStateType = {
  boards: [],
};
