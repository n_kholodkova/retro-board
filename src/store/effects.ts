import Board from '../models/board.model';
import { Injectable } from '@angular/core';
import { createEffect, ofType, Actions } from '@ngrx/effects';
import { from } from 'rxjs';
import { switchMap, map, mergeMap, concatMap } from 'rxjs/operators';

import {
  GetBoardsAction,
  AddBoardAction,
  AddBoardItemAction,
  AddCommentAction,
  DeleteBoardAction,
  DeleteBoardItemAction,
  LikeAction,
  DislikeAction,
  ImportantAction,
  ChangeColorAction,
  SetBoardsAction,
} from './actions';
import { BoardsService } from '../services/boards.service';

@Injectable()
export class BoardsEffects {
  getBoards$ = createEffect(() =>
    this.actions$.pipe(
      ofType(GetBoardsAction),
      switchMap(() => {
        return from(this.boardsService.getBoards()).pipe(
          map((boards: Board[]) => {
            return SetBoardsAction({ payload: { boards } });
          })
        );
      })
    )
  );

  addBoardItem$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AddBoardItemAction),
      concatMap(({ payload }) => {
        return this.boardsService.getBoard(payload.id).pipe(
          concatMap((board) => {
            return this.boardsService
              .addBoardItem(board, payload.title, payload.id)
              .pipe(map(() => GetBoardsAction()));
          })
        );
      })
    )
  );

  addBoard$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AddBoardAction),
      concatMap(({ payload }) => {
        return from(this.boardsService.addBoard(payload.title)).pipe(
          map(() => {
            return GetBoardsAction();
          })
        );
      })
    )
  );

  deleBoardItem$ = createEffect(() =>
    this.actions$.pipe(
      ofType(DeleteBoardItemAction),
      concatMap(({ payload }) => {
        return this.boardsService.getBoard(payload.id).pipe(
          concatMap((board) => {
            return this.boardsService
              .deleteBoardItem(payload.index, board, payload.id)
              .pipe(map(() => GetBoardsAction()));
          })
        );
      })
    )
  );

  deleteBoard$ = createEffect(() =>
    this.actions$.pipe(
      ofType(DeleteBoardAction),
      switchMap(({ payload }) => {
        return from(this.boardsService.deleteBoard(payload.id)).pipe(
          map(() => {
            return GetBoardsAction();
          })
        );
      })
    )
  );

  addComment$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AddCommentAction),
      concatMap(({ payload }) => {
        return this.boardsService.getBoard(payload.id).pipe(
          concatMap((board) => {
            return this.boardsService
              .addComment(payload.index, payload.title, board, payload.id)
              .pipe(map(() => GetBoardsAction()));
          })
        );
      })
    )
  );

  addLike$ = createEffect(() =>
    this.actions$.pipe(
      ofType(LikeAction),
      concatMap(({ payload }) => {
        return this.boardsService.getBoard(payload.id).pipe(
          concatMap((board) => {
            return this.boardsService
              .like(payload.index, board, payload.id)
              .pipe(map(() => GetBoardsAction()));
          })
        );
      })
    )
  );

  addDisLike$ = createEffect(() =>
    this.actions$.pipe(
      ofType(DislikeAction),
      concatMap(({ payload }) => {
        return this.boardsService.getBoard(payload.id).pipe(
          concatMap((board) => {
            return this.boardsService
              .dislike(payload.index, board, payload.id)
              .pipe(map(() => GetBoardsAction()));
          })
        );
      })
    )
  );

  markImportant$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ImportantAction),
      concatMap(({ payload }) => {
        return this.boardsService.getBoard(payload.id).pipe(
          concatMap((board) => {
            return this.boardsService
              .markImportant(payload.index, board, payload.id)
              .pipe(map(() => GetBoardsAction()));
          })
        );
      })
    )
  );

  changeColor$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ChangeColorAction),
      concatMap(({ payload }) => {
        return this.boardsService.getBoard(payload.id).pipe(
          concatMap((board) => {
            return this.boardsService
              .changeColor(board, payload.color, payload.id)
              .pipe(map(() => GetBoardsAction()));
          })
        );
      })
    )
  );

  constructor(
    private boardsService: BoardsService,
    private actions$: Actions
  ) {}
}
